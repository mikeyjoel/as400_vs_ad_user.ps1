<#

Author: Michael J. Acosta
Date: 10/21/2021

About: Compare Full Name or Username to MS AD User Objects and output a csv with information for MS AD Account Enabled, Last Login, Extension in AD, Others..

Requirements: A csv with the following colums (Notice how the Reports To Manager ID has a space at the end on the original xls file.)
                    
                    "IBMID"
                    "First Name"
                    "Last Name"



How to run: \as400_vs_ad_userobj.ps1 <path to csv>

Log: 

Tips:
You can filter and compare data against the log using the commands below.

        --Description--                                        --Command--
    
    View One Page at a Time:                     Get-Content .\as400_vs_ad_userobj.log | More
    Display Errors:              Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "Error" | More
    Filter Specific User:                        Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "<Username>" | More
    Count How Many Users Errors:                 (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "Error").count
    Count How Many Users Enabled:                (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "True").count
                                                 (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "False").count
    Count How Many Users Error:                  (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "Error").count

    
#>

param (
  [Parameter(Position=0,Mandatory=$true,HelpMessage='CSV file containing Collumns ''First Name'',''Last Name'' & ''PBX Extension'' ')]
  [ValidateNotNullOrEmpty()]
  [string]$CSV
);

$Users = Import-Csv $CSV

function Write-Log {
    [CmdletBinding()]
    param(
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$Message,
 
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [ValidateSet('Information','Warning','Error','Critical','ManagerSkipped')]
        [string]$Severity = 'Information'
    )
    $LogPath  = ".\as400_vs_ad_userobj.log"
    [pscustomobject]@{
        Time = (Get-Date -f g)
        Message = $Message
        Severity = $Severity
    } | Add-Content -Path $LogPath
 }

 function StartLog {
     
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "          START OF USR COMPARISON LOG              "
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "---------------------------------------------------"
     
 }

function compareUsers{
    foreach ($User in $Users) {

        #Get csv object from each row/column
        $EmpFN = $User."First Name"
        $EmpLN = $User."Last Name"
        
        $IBMID = $User."IBMID"
        

        $FullName = "$EmpFN $EmpLN"

        #Used for confirming that the user was filtered using DisplayName or Full Name Filtered to First 4 characters of the First and Last Name.
        $Fuzzy = "True"
        $NFuzzy = "False"
       
        #Mikey* (Reminder for myself) that Powershell Core/Desktop: Using the built in Substring method is not reliable for this task after additional lab research.
        #$EmpFNC = ($EmpFN).SubString(0,4)
        #$EMPLNC = ($EmpLN).SubString(0,4)
        #Let's use Regex instead ;)

        #Convert First Name and Last Name to UPN AD Object. 
        $EmpFNC = $EmpFN -replace '\-','' -replace '\s','' -replace '\`','' -replace '\''','' -replace '\~','' -replace '\,','' -replace '\.','' `
        | Select-String -Pattern '^.{0,4}' | ForEach-Object { $_.Matches } | Select-Object -ExpandProperty Value
        $EMPLNC = $EmpLN -replace '\-','' -replace '\s','' -replace '\`','' -replace '\''','' -replace '\~','' -replace '\,','' -replace '\.','' `
        | Select-String -Pattern '^.{0,4}' | ForEach-Object { $_.Matches } | Select-Object -ExpandProperty Value
        $EMPUPN = $EmpFNC.Trim()+$EmpLNC.Trim()


        try {

            $UPN = $null
            $EmailAddress = $null
            $ADExt = $null
            $Enabled = $null
            $LockedOut = $null
            $LastLogin = $null

            $UPN = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty UserPrincipalName -OutVariable UserPrincipalName
            $EmailAddress = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty EmailAddress -OutVariable EmailAddress
            $ADExt = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty OfficePhone -OutVariable OfficePhone
            $Enabled = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty Enabled -OutVariable Enabled
            $LockedOut = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty LockedOut -OutVariable LockedOut
            $LastLogin = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty LastLogonDate -OutVariable LastLogonDate
            $SAM = Get-ADUser -Identity $IBMID -Properties * | Select-Object -ExpandProperty SamAccountName -OutVariable SamAccountName
            


             Write-Log -Message "$IBMID $Enabled" -Severity Information
             
             Add-Content -Path .\as400_vs_ad_userobj.csv -Value $IBMID","$SAM","$EmpFN","$EmpLN","$UPN","$EmailAddress","$ADExt","$Enabled","$LockedOut","$LastLogin","$NFuzzy
        }
        catch {
            Write-Log -Message " $FullName | $IBMID not found in AD with IBMID. " -Severity Error
            Write-Log -Message " $FullName | $IBMID Checking via Fuzzy UPN Pattern... " -Severity Information

            try {

                $UPN = $null
                $EmailAddress = $null
                $ADExt = $null
                $Enabled = $null
                $LockedOut = $null
                $LastLogin = $null

                $UPN = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty UserPrincipalName -OutVariable UserPrincipalName
                $EmailAddress = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty EmailAddress -OutVariable EmailAddress
                $ADExt = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty OfficePhone -OutVariable OfficePhone
                $Enabled = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty Enabled -OutVariable Enabled
                $LockedOut = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty LockedOut -OutVariable LockedOut
                $LastLogin = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty LastLogonDate -OutVariable LastLogonDate
                $SAM = Get-ADUser -Identity $EmpUPN -Properties * | Select-Object -ExpandProperty SamAccountName -OutVariable SamAccountName
                


                Write-Log -Message "$EmpUPN $Enabled" -Severity Information
                
                Add-Content -Path .\as400_vs_ad_userobj.csv -Value $IBMID","$SAM","$EmpFN","$EmpLN","$UPN","$EmailAddress","$ADExt","$Enabled","$LockedOut","$LastLogin","$Fuzzy
    
            }
            catch {
                Write-Log -Message " $FullName | $EMPUPN not found in AD with Fuzzy Patterened UPN. " -Severity Error
                Write-Log -Message " $FullName | $EMPUPN Checking via Fuzzy DisplayName... " -Severity Information

                try {

                    $UPN = $null
                    $EmailAddress = $null
                    $ADExt = $null
                    $Enabled = $null
                    $LockedOut = $null
                    $LastLogin = $null
                    
                    $Enabled = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty Enabled -OutVariable Enabled
                    $EmailAddress = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty EmailAddress -OutVariable EmailAddress
                    $ADExt = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty OfficePhone -OutVariable OfficePhone
                    $LastLogin = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty LastLogonDate -OutVariable LastLogonDate
                    $UPN = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty UserPrincipalName -OutVariable UserPrincipalName
                    $SAM = Get-ADUser -Filter {DisplayName -eq $FullName} -Properties * | Select-Object -ExpandProperty SamAccountName -OutVariable SamAccountName
        
        
                    Write-Log -Message "$EmpUPN $Enabled" -Severity Information
                    
                    Add-Content -Path .\as400_vs_ad_userobj.csv -Value $IBMID","$SAM","$EmpFN","$EmpLN","$UPN","$EmailAddress","$ADExt","$Enabled","$LockedOut","$LastLogin","$Fuzzy
                    }
                catch {
                    Write-Log -Message " ! $FullName | $EMPUPN Not found with Full Display Name, Skipping ! " -Severity Critical
                }

            }
        }
    }
}

function EndLog {
     
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "          END OF USR COMPARISON LOG                "
    Write-Log -Message "---------------------------------------------------"
    Write-Log -Message "---------------------------------------------------"
     
 }

 function cleanupCSV {
        try {
            Remove-Item -Path .\as400_vs_ad_userobj.csv -ErrorAction SilentlyContinue
        }
        catch {
           Write-Host "Creating CSV..."
        }
        
        try {
            Remove-Item -Path .\as400_vs_ad_userobj.log -ErrorAction SilentlyContinue
        }
        catch {
            Write-Host "Creating Log..."
        }
        
        Add-Content -Path .\as400_vs_ad_userobj.csv -Value "IBMID,SamAccountName,First Name,Last Name,UserPrincipalName,Email Address,Ext,Account Enabled,LockedOut,Last Login,Fuzzy"
        
 }
 
 
cleanupCSV
StartLog
compareUsers
EndLog

Exit 0