Author: Michael J. Acosta
Date: 11/30/2021

About: Compare Full Name or Username to MS AD User Objects and output a csv with information for MS AD Account Enabled, Last Login, Extension in AD, Others..

Logic:
    First, the IBMDID is used for checking if that user exists in AD.
    If not, it will then use the first 4 characters of the First and Last Name.
    Finally if it was still not found, it will try using the Full name as Display Name in AD.

Requirements: A csv with the following colums:
                    
    "IBMID"
    "First Name"
    "Last Name"



How to run: \as400_vs_ad_userobj.ps1 filename.csv

Csv: as400_vs_ad_userobj.csv
Log: as400_vs_ad_userobj.log

Tips:
You can filter and compare data against the log using the commands below.

        --Description--                                        --Command--
    
    View One Page at a Time:                     Get-Content .\as400_vs_ad_userobj.log | More
    Display Errors:              Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "Error" | More
    Filter Specific User:                        Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "<Username>" | More
    Count How Many Users Errors:                 (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "Error").count
    Count How Many Users Enabled:                (Get-Content .\as400_vs_ad_userobj.log | Select-String -Pattern "True").count
    
    
This new release has a new 'Fuzzy' column that will warn the operator if the user was found using a search pattern other than the IBMID.

The output header should provide the following in order:

    IBMID,SamAccountName,First Name,Last Name,UserPrincipalName,Email Address,Ext,Account Enabled,LockedOut,Last Login,Fuzzy

    
